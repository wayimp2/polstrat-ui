import React from "react"
import AdminContainerWrapper from "./AdminContainerWrapper"

const AdminContainer = ({ title, subTitle, children }) => {
  return (
    <AdminContainerWrapper>
      <div className="hub-header">
        <div className="hub-title">
          <h3 className="share-tech-mono">{title}</h3>
        </div>
        <div className="hub-subheader">
          <div className="hub-subtitle">
            <div className="hub-subtitle-display">
              <h2 className="share-tech-mono">{subTitle}</h2>
            </div>
          </div>
        </div>
      </div>
      <div className="hub-body">{children}</div>
    </AdminContainerWrapper>
  )
}

export default AdminContainer
