import Request from '../../request'
import { setLoading } from './components'
import { GET_SURVEY, GET_SURVEY_ERROR, UPDATE_SURVEY } from '../types'

// GET SURVEY

export const dispatchGetSurvey = (data, count) => {
  return async dispatch => {
    try {
      await dispatch({ type: GET_SURVEY, payload: { data, count } })
      await dispatch(setLoading(false))
    } catch (error) {}
  }
}

export const getSurveyError = () => {
  return async dispatch => {
    try {
      await dispatch({ type: GET_SURVEY_ERROR })
      await dispatch(setLoading(false))
    } catch (error) {}
  }
}

export const getSurveys = () => {
  return async dispatch => {
    try {
      await dispatch(setLoading(true))
      const request = new Request()
      const resInfo = await request.get({ url: `/survey/all` })

      if (!resInfo || !resInfo.data) {
        await dispatch(getSurveyError())
      }

      await dispatch(dispatchGetSurvey(resInfo.data, resInfo.data.count))
    } catch (error) {
      await dispatch(getSurveyError())
    }
  }
}

export const getSurvey = name => {
  return async dispatch => {
    try {
      await dispatch(setLoading(true))
      const request = new Request()
      const resInfo = await request.get({ url: `/survey/${name}` })

      if (!resInfo || !resInfo.data) {
        await dispatch(getSurveyError())
      }

      await dispatch(dispatchGetSurvey(resInfo.data, resInfo.data.count))
    } catch (error) {
      await dispatch(getSurveyError())
    }
  }
}

export const updateSurvey = survey => {
  return async dispatch => {
    try {
      await dispatch(setLoading(true))
      const request = new Request()
      const resInfo = await request.put({
        url: `/survey/update/${survey._id}`,
        data: survey
      })
      dispatch(setLoading(false))
    } catch (error) {
      await dispatch(getSurveyError())
      dispatch(setLoading(false))
    }
  }
}
