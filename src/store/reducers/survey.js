import { GET_SURVEY } from "../types"

export const surveyState = {
  data: {
    ready: false,
  },
  fetched: [],
}

export const survey = (state = surveyState, action) => {
  switch (action.type) {
    case GET_SURVEY:
      return {
        ...state,
        fetched: 
          [...action.payload.data]
        ,
      }

    default:
      return state
  }
}
