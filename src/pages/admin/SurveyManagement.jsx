import React, { useEffect, useState, lazy, Suspense } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getUser } from '../../store/actions/user'
import AdminContainer from '../../components/containers/AdminContainer/AdminContainer'
import SpinnerComponent from '../../components/elements/SpinnerComponent'
import { Button } from 'primereact/button'
import { Link } from 'react-router-dom'
import { confirmDialog } from 'primereact/confirmdialog'
import {
  HeaderWrapper,
  List,
  Kiosk,
  Item,
  Clone
} from './SurveyManagementWrapper.js'
import { getSurveys, updateSurvey } from '../../store/actions/survey'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'
import { Dialog } from 'primereact/dialog'
import { InputText } from 'primereact/inputtext'
import QuestionComponent from '../../components/QuestionComponent'
import { Dropdown } from 'primereact/dropdown'
import { Toolbar } from 'primereact/toolbar'
import { Fieldset } from 'primereact/fieldset'
import { uuid } from 'uuidv4'
const grid = 8

const QUESTION_TEMPLATE = {
  sectionTitle: 'QUESTION_TEMPLATE',
  items: [
    {
      id: uuid(),
      type: 'html',
      label: 'HTML'
    },
    {
      id: uuid(),
      type: 'text',
      label: 'Text'
    },
    {
      id: uuid(),
      type: 'number',
      label: 'Number'
    },
    {
      id: uuid(),
      type: 'radio',
      label: 'Radio',
      options: []
    },
    {
      id: uuid(),
      type: 'dropdown',
      label: 'DropDown',
      placeholder: '',
      options: []
    }
  ]
}

const dummySurvey = {}
/*{
  name: 'Personal',
  structure: [
    {
      sectionTitle: 'Personal Information',
      items: [
        {
          id: uuid(),
          type: 'html',
          html:
            '<p>This is a general description for this section of the survey.<p>',
          visibility: 'public'
        },
        {
          id: uuid(),
          type: 'radio',
          prompt: 'Sex',
          defaultValue: 'Male',
          visibility: 'public',
          options: [
            { label: 'Male', value: 'Male' },
            { label: 'Female', value: 'Female' }
          ]
        },
        {
          id: uuid(),
          type: 'number',
          prompt: 'Your Age',
          defaultValue: '',
          visibility: 'public'
        },
        {
          id: uuid(),
          type: 'number',
          prompt: 'What age gap in years is acceptable for you?',
          defaultValue: '',
          visibility: 'public'
        },
        {
          id: uuid(),
          type: 'dropdown',
          prompt: 'If you would prefer to be less specific',
          placeholder: 'Age Group',
          visibility: 'public',
          options: [
            { label: 'Under 12 years old', value: '<12' },
            { label: '12-17 years old', value: '12-17' },
            { label: '18-24 years old', value: '18-24' },
            { label: '25-34 years old', value: '25-34' },
            { label: '35-44 years old', value: '35-44' },
            { label: '45-54 years old', value: '45-54' },
            { label: '55-64 years old', value: '55-64' },
            { label: '65-74 years old', value: '65-74' },
            { label: '75 years or older', value: '75+' }
          ]
        }
      ]
    }
  ]
}
*/
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list)
  const [removed] = result.splice(startIndex, 1)
  result.splice(endIndex, 0, removed)
  return result
}

const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source.items)
  const destClone = Array.from(destination.items)
  const [removed] = sourceClone.splice(droppableSource.index, 1)

  destClone.splice(droppableDestination.index, 0, removed)

  const result = {}

  result[droppableSource.droppableId] = {
    sectionTitle: source.sectionTitle,
    items: sourceClone
  }

  result[droppableDestination.droppableId] = {
    sectionTitle: destination.sectionTitle,
    items: destClone
  }

  return result
}

const copy = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source.items)
  const destClone = Array.from(destination.items)
  const item = sourceClone[droppableSource.index]

  destClone.splice(droppableDestination.index, 0, { ...item, id: uuid() })

  const result = {}

  result[droppableDestination.droppableId] = {
    sectionTitle: destination.sectionTitle,
    items: destClone
  }

  return result
}

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: 'none',
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,

  // change background colour if dragging
  background: isDragging ? 'lightgreen' : 'black',

  // styles we need to apply on draggables
  ...draggableStyle
})

const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? 'lightblue' : 'lightgrey',
  padding: grid
})

const SurveyManagement = ({ props, data }) => {
  const dispatch = useDispatch()
  // this state is needed for triggering the loading spinner
  const [loadingStatus, setLoadingStatus] = useState(null)
  const [sectionEditDialog, setSectionEditDialog] = useState(false)
  const [sectionEditIndex, setSectionEditIndex] = useState(-1)
  const [sectionEditTitle, setSectionEditTitle] = useState('')
  const [questionConfig, setQuestionConfig] = useState({})
  const [questionConfigDialog, setQuestionConfigDialog] = useState(false)
  const [state, setState] = useState([])
  const [selectedSurvey, setSelectedSurvey] = useState({})
  const { fetched } = useSelector(state => state.survey)

  useEffect(() => dispatch(getSurveys()), [])

  const handleSurveyChange = e => {
    const survey = fetched.find(s => s.value === e.value)
    setSelectedSurvey(survey)
    setState(survey.sections)
  }

  const saveCurrent = () => {
    const updated = { ...selectedSurvey, sections: state }
    dispatch(updateSurvey(updated))
  }

  function onDragEnd (result) {
    const { source, destination } = result

    // dropped outside the list
    if (!destination) {
      return
    }

    if (destination.droppableId === 'QUESTION_TEMPLATE') {
      return
    }

    const sInd = +source.droppableId
    const dInd = +destination.droppableId

    if (source.droppableId === 'QUESTION_TEMPLATE') {
      const result = copy(QUESTION_TEMPLATE, state[dInd], source, destination)
      const newState = [...state]
      newState[dInd] = result[dInd]
      setState(newState)
    } else {
      if (sInd === dInd) {
        const items = reorder(
          state[sInd].items,
          source.index,
          destination.index
        )
        const newState = [...state]
        newState[sInd].items = items
        setState(newState)
      } else {
        const result = move(state[sInd], state[dInd], source, destination)
        const newState = [...state]
        newState[sInd] = result[sInd]
        newState[dInd] = result[dInd]
        setState(newState)
      }
    }
  }

  const header = (
    <HeaderWrapper>
      <div className='header-description'>
        <div className='header-title'>
          <div className='header-name'>
            <h1 className='share-tech-mono'></h1>
          </div>
        </div>
      </div>
      <div className='header-menu'>
        <div className='menu-buttons'></div>
      </div>
    </HeaderWrapper>
  )

  return (
    <AdminContainer title={<div>Survey Management</div>} header={header}>
      <Toolbar
        left={
          <Dropdown
            value={selectedSurvey.value}
            options={fetched}
            onChange={handleSurveyChange}
            optionLabel='label'
            placeholder='Select Survey'
          />
        }
        right={
          <Button
            label='Save Current Survey'
            icon='pi pi-check'
            className='p-button-success'
            onClick={saveCurrent}
          />
        }
      />

      <div>
        <div className='p-grid'>
          <div className='p-col-3 p-m-3'>
            <Button
              label='Add Survey Section'
              className='p-button-outlined p-button-sm p-button-secondary'
              icon='pi pi-book'
              onClick={() => {
                setSectionEditIndex(-1)
                setSectionEditTitle('')
                setSectionEditDialog(true)
              }}
            />
          </div>
        </div>

        <DragDropContext onDragEnd={onDragEnd} isDropDisabled={true}>
          <div className='p-grid'>
            <div className='p-col-1'>
              <Fieldset legend='Available Controls'>
                <Droppable key={99} droppableId={'QUESTION_TEMPLATE'}>
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      style={getListStyle(snapshot.isDraggingOver)}
                      {...provided.droppableProps}
                      innerRef={provided.innerRef}
                      isDraggingOver={snapshot.isDraggingOver}
                    >
                      {QUESTION_TEMPLATE.items.map((template, index) => (
                        <Draggable
                          key={template.id}
                          draggableId={template.id}
                          index={index}
                        >
                          {(provided, snapshot) => (
                            <>
                              <Item
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                style={getItemStyle(
                                  snapshot.isDragging,
                                  provided.draggableProps.style
                                )}
                              >
                                {template.label}
                              </Item>
                              {snapshot.isDragging ? (
                                <Clone>{template.label}</Clone>
                              ) : (
                                ''
                              )}
                            </>
                          )}
                        </Draggable>
                      ))}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </Fieldset>
            </div>
            <div className='p-col-7'>
              {state.map((el, ind) => (
                <div key={`builder-${ind}`}>
                  <Fieldset
                    legend={
                      <div>
                        <h5>{el.sectionTitle}</h5>
                        <Button
                          label='Edit'
                          className='p-button-outlined p-button-sm p-button-secondary'
                          icon='pi pi-book'
                          onClick={() => {
                            setSectionEditIndex(ind)
                            setSectionEditTitle(el.sectionTitle)
                            setSectionEditDialog(true)
                          }}
                        />
                        <Button
                          label='Delete Section'
                          className='p-button-outlined p-button-sm p-button-secondary'
                          icon='pi pi-minus-circle'
                          onClick={() => {
                            const newState = [...state]
                            newState.splice(ind, 1)
                            setState(newState)
                          }}
                        />
                      </div>
                    }
                  >
                    <Droppable key={ind} droppableId={`${ind}`}>
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          style={getListStyle(snapshot.isDraggingOver)}
                          {...provided.droppableProps}
                        >
                          {el.items.map((item, index) => (
                            <Draggable
                              key={item.id}
                              draggableId={item.id}
                              index={index}
                            >
                              {(provided, snapshot) => (
                                <div
                                  ref={provided.innerRef}
                                  {...provided.draggableProps}
                                  {...provided.dragHandleProps}
                                  style={getItemStyle(
                                    snapshot.isDragging,
                                    provided.draggableProps.style
                                  )}
                                >
                                  <div>
                                    <QuestionComponent
                                      props={item}
                                      mode='entry'
                                      updateValue={value => {
                                        const newState = [...state]
                                        newState[ind].items[index].value = value
                                        setState(newState)
                                      }}
                                    />
                                    <Button
                                      label='Edit'
                                      className='p-button-outlined p-button-sm p-button-secondary'
                                      icon='pi pi-window-maximize'
                                      onClick={() => {
                                        setQuestionConfig(
                                          <QuestionComponent
                                            props={item}
                                            mode='config'
                                            updateConfig={json => {
                                              setQuestionConfigDialog(false)
                                              if (json) {
                                                const newState = [...state]
                                                newState[ind].items[
                                                  index
                                                ] = json
                                                setState(newState)
                                              }
                                            }}
                                          />
                                        )
                                        setQuestionConfigDialog(true)
                                      }}
                                    />
                                    <Button
                                      label='Delete'
                                      className='p-button-outlined p-button-sm p-button-secondary'
                                      icon='pi pi-minus-circle'
                                      onClick={() => {
                                        const newState = [...state]
                                        newState[ind].items.splice(index, 1)
                                        setState(newState)
                                      }}
                                    />
                                  </div>
                                </div>
                              )}
                            </Draggable>
                          ))}
                          {provided.placeholder}
                        </div>
                      )}
                    </Droppable>
                  </Fieldset>
                </div>
              ))}
            </div>
            <div className='p-col-4'>
              <Fieldset legend='Data-Entry Mode'>
                {state.map((el, ind) => (
                  <div key={`entry-${ind}`}>
                    <Fieldset legend={el.sectionTitle}>
                      {el.items.map((item, index) => (
                        <QuestionComponent
                          props={item}
                          mode='entry'
                          updateValue={value => {
                            const newState = [...state]
                            newState[ind].items[index].value = value
                            setState(newState)
                          }}
                        />
                      ))}
                    </Fieldset>
                  </div>
                ))}
              </Fieldset>
              <Fieldset legend='Read-Only Display' style={{ marginTop: 20 }}>
                {state.map((el, ind) => (
                  <div key={`display-${ind}`}>
                    <Fieldset legend={el.sectionTitle}>
                      {el.items.map((item, index) => (
                        <QuestionComponent props={item} mode='display' />
                      ))}
                    </Fieldset>
                  </div>
                ))}
              </Fieldset>
            </div>
          </div>
        </DragDropContext>
      </div>
      <Dialog
        header='Edit Question'
        visible={questionConfigDialog}
        style={{ width: '50vw' }}
        onHide={() => setQuestionConfigDialog(false)}
      >
        {questionConfig}
      </Dialog>
      <Dialog
        header='Edit Section'
        visible={sectionEditDialog}
        style={{ width: '50vw' }}
        footer={
          <div>
            <Button
              label='Cancel'
              icon='pi pi-times'
              onClick={() => {
                setSectionEditIndex(-1)
                setSectionEditTitle('')
                setSectionEditDialog(false)
              }}
              className='p-button-text'
            />
            <Button
              label='OK'
              icon='pi pi-check'
              onClick={() => {
                if (sectionEditIndex < 0) {
                  setState([
                    ...state,
                    { sectionTitle: sectionEditTitle, items: [] }
                  ])
                } else {
                  const newState = JSON.parse(JSON.stringify(state))
                  newState[sectionEditIndex].sectionTitle = sectionEditTitle
                  setState(newState)
                }
                setSectionEditIndex(-1)
                setSectionEditTitle('')
                setSectionEditDialog(false)
              }}
              autoFocus
            />
          </div>
        }
        onHide={() => setSectionEditDialog(false)}
      >
        <div className='p-field'>
          <label htmlFor='sectionEditTitle' className='p-d-block'>
            Title
          </label>
          <InputText
            id='sectionEditTitle'
            className='p-d-block'
            value={sectionEditTitle}
            onChange={e => setSectionEditTitle(e.target.value)}
          />
        </div>
      </Dialog>
    </AdminContainer>
  )
}

export default SurveyManagement
