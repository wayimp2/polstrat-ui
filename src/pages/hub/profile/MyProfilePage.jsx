import React from 'react'
import ProfileContainer from '../../../components/containers/ProfileContainer/ProfileContainer'
import { Panel } from 'primereact/panel'
import ProfilePage from '../../userProfile/UserProfilePage'

const Profile = () => {
  return (
    <ProfileContainer pageTitle='Profile' target='My Profile'>
      <Panel header='Basic Information'>
        <ProfilePage/>
      </Panel>
    </ProfileContainer>
  )
}

export default Profile
